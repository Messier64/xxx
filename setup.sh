#!/bin/bash

read -p 'Password: ' pass
n=$(nmcli con | grep Wired | awk '{print $4}')
echo $n
nmcli con mod $n ipv4.dns "1.1.1.1 1.0.0.1"
nmcli con mod $n ipv6.dns "2606:4700:4700::1111 2606:4700:4700::1001"

echo $pass | sudo -kS pacman -Syu archlinux-keyring --noconfirm

echo $pass | sudo -kS pacman -S nvidia-prime nvidia nvidia-settings amd-ucode xf86-video-amdgpu vulkan-radeon polybar sxhkd rofi kitty chromium playerctl bc firefox xorg-xinit xorg-server xorg-xset xorg-xsetroot feh lightdm nautilus networkmanager papirus-icon-theme ttf-font-awesome ttf-hack xclip python3 xsettingsd lxappearance steam mpv bspwm inotify-tools lightdm-gtk-greeter lib32-mesa picom mpv xdotool qpwgraph --noconfirm

mkdir -pv $HOME/.config $HOME/screenshots $HOME/code $HOME/isos

ln -sfv $HOME/.dotfiles/.config/bspwm $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/sxhkd $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/kitty $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/rofi $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/polybar $HOME/.config/
ln -sfv $HOME/.dotfiles/.config/picom $HOME/.config/
ln -sfv $HOME/.dotfiles/chrome $HOME/.mozilla/firefox/*.default-release/
ln -sfv $HOME/.dotfiles/.scripts $HOME/
ln -sfv $HOME/.dotfiles/.xprofile $HOME/
ln -sfv $HOME/.dotfiles/.wallpapers $HOME/
ln -sfv $HOME/.dotfiles/.gitconfig $HOME/

mkdir -pv $HOME/.local/bin

touch $HOME/.Xresources

echo $pass | sudo -kS sed -i 's/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/g' /etc/mkinitcpio.conf
echo $pass | sudo -kS mkinitcpio -p linux
echo $pass | sudo -kS mkdir -p /etc/pacman.d/hooks
echo $pass | sudo -kS cp -r $HOME/.dotfiles/.nvidia/nvidia.hook /etc/pacman.d/hooks/
echo $pass | sudo -kS cp -r $HOME/.dotfiles/.nvidia/70-nvidia.rules /etc/udev/rules.d/
echo $pass | sudo -kS cp -r $HOME/.dotfiles/play $HOME/.local/bin/

git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg
echo $pass | sudo -kS pacman -U *.pkg.tar.* --noconfirm
cd ..
echo $pass | sudo -kS rm -rf paru-bin

echo $pass | sudo -S paru -Ss polybar
paru -S papirus-folders-git wpgtk ttf-twemoji spotify spotify-adblock-git plymouth lxsession zscroll-git arandr ryzenadj fastfetch optimus-manager optimus-manager-qt noisetorch --noconfirm

echo $pass | sudo -kS systemctl enable lightdm
papirus-folders -C black
wpg-install.sh -g
wpg -a $HOME/.wallpapers/901676.jpg
wpg -s 901676.jpg
reboot

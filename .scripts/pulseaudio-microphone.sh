#!/bin/sh

status() {
  MUTED=$(pactl list sources | awk '/Mute/ {print $2;}' | sed -n '2p')

  if [ "$MUTED" = "yes" ]; then
    echo "muted"
  else
    echo "unmuted"
  fi
}

listen() {
    status

    LANG=EN; pactl subscribe | while read -r event; do
        if echo "$event" | grep -q "source" || echo "$event" | grep -q "server"; then
            status
        fi
    done
}
DEFAULT_SOURCE=$(pactl list sources | awk '/Name/ {print $2;}' | sed -n '2p')
increase() {
  pactl set-source-volume "$DEFAULT_SOURCE" +5%
}

decrease() {
  pactl set-source-volume "$DEFAULT_SOURCE" -5%
}

case "$1" in
    --toggle)
        toggle
        ;;
    --increase)
        increase
        ;;
    --decrease)
        decrease
        ;;
    *)
        listen
        ;;
esac

#!/bin/bash

zscroll --delay 0.3 \
		--match-command "xdotool getwindowfocus getwindowname" \
		--update-check true "xdotool getwindowfocus getwindowname" &

wait

